/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.webdriver;

import java.util.*;
import java.util.logging.*;

import org.marvelution.testing.utils.*;

import com.codeborne.selenide.*;
import com.codeborne.selenide.impl.*;
import org.junit.jupiter.api.extension.*;
import org.openqa.selenium.*;
import org.openqa.selenium.logging.*;
import org.slf4j.Logger;
import org.slf4j.*;

import static com.codeborne.selenide.WebDriverRunner.*;
import static java.io.File.*;

/**
 * {@link Extension} for handling initialization of the {@link WebDriver} and setting contexts for screenshots.
 *
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class WebDriverExtension
        implements ParameterResolver, BeforeAllCallback, BeforeEachCallback, AfterEachCallback, AfterAllCallback
{

    private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(WebDriver.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverExtension.class);

    @Override
    public boolean supportsParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
    {
        return parameterContext.getParameter()
                       .getType() == WebDriver.class;
    }

    @Override
    public Object resolveParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
    {
        return getOrCreateWebDriver(extensionContext);
    }

    @Override
    public void beforeAll(ExtensionContext context)
    {
        getOrCreateWebDriver(context);
    }

    @Override
    public void beforeEach(ExtensionContext context)
    {
        ScreenShotLaboratory.getInstance()
                .startContext(context.getRequiredTestClass()
                                      .getName() + separator + context.getRequiredTestMethod()
                                      .getName() + separator);
    }

    @Override
    public void afterEach(ExtensionContext context)
    {
        List<Screenshot> screenshots = ScreenShotLaboratory.getInstance()
                .finishContext();
        if (!screenshots.isEmpty())
        {
            LOGGER.info("Shot {} screenshots:", screenshots.size());
            screenshots.forEach(screenshot -> LOGGER.info(" - {}", screenshot));
        }

        WebDriver webDriver = context.getStore(NAMESPACE)
                .get(WebDriver.class, WebDriver.class);
        if (webDriver != null)
        {
            Logs logs = webDriver.manage()
                    .logs();

            for (String logType : logs.getAvailableLogTypes())
            {
                List<LogEntry> logEntries = logs.get(logType)
                        .getAll();
                if (!logEntries.isEmpty())
                {
                    LOGGER.info("--- START OF LOG: {} ---", logType);
                    logEntries.forEach(entry -> LOGGER.info("{} {} {}",
                            new Date(entry.getTimestamp()),
                            entry.getLevel(),
                            entry.getMessage()));
                    LOGGER.info("--- END OF LOG: {} ---", logType);
                }
            }
        }
    }

    @Override
    public void afterAll(ExtensionContext context)
    {
        closeWebDriver();
    }

    private WebDriver getOrCreateWebDriver(ExtensionContext extensionContext)
    {
        return extensionContext.getStore(NAMESPACE)
                .getOrComputeIfAbsent(WebDriver.class, webDriverClass -> {
                    LoggingPreferences loggingPrefs = new LoggingPreferences();
                    loggingPrefs.enable(LogType.BROWSER, Level.ALL);
                    loggingPrefs.enable(LogType.PERFORMANCE, Level.ALL);
                    Configuration.browserCapabilities.setCapability("goog:loggingPrefs", loggingPrefs);
                    // Resolve to non-localhost base url if needed just before creating the WebDriver
                    BaseUrl.setBaseUrl(Configuration.baseUrl);
                    return getAndCheckWebDriver();
                }, WebDriver.class);
    }
}
