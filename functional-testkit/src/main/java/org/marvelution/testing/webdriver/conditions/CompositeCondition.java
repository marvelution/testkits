/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.webdriver.conditions;

import javax.annotation.*;
import java.util.*;

import com.codeborne.selenide.*;
import com.codeborne.selenide.ex.*;
import com.codeborne.selenide.impl.*;
import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.*;
import static java.util.Arrays.*;

/**
 * @author Mark Rekveld
 * @since 1.3.0
 */
public class CompositeCondition
        extends CollectionCondition
{

    private final List<? extends Condition> conditions;
    private boolean ordered;

    private CompositeCondition(List<? extends Condition> conditions)
    {
        this.conditions = conditions;
    }

    public static CompositeCondition composite(Condition... conditions)
    {
        return composite(asList(conditions));
    }

    public static CompositeCondition composite(List<? extends Condition> conditions)
    {
        return new CompositeCondition(conditions);
    }

    public CompositeCondition ordered(boolean ordered)
    {
        this.ordered = ordered;
        return this;
    }

    @Override
    public boolean test(List<WebElement> elements)
    {
        if (elements.size() != conditions.size())
        {
            return false;
        }
        if (ordered)
        {
            for (int i = 0; i < conditions.size(); i++)
            {
                SelenideElement element = $(elements.get(i));
                Condition condition = conditions.get(i);
                if (!element.is(condition))
                {
                    return false;
                }
            }
        }
        else
        {
            for (WebElement element : elements)
            {
                if (!apply($(element)))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean apply(SelenideElement element)
    {
        for (Condition condition : conditions)
        {
            if (element.is(condition))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean missingElementSatisfiesCondition()
    {
        return false;
    }

    @Override
    public void fail(
            CollectionSource collection,
            @Nullable
            List<WebElement> elements,
            @Nullable
            Exception lastError,
            long timeoutMs)
    {
        StringJoiner actualJoiner = new StringJoiner("]\n[", "Actual: \n[", "].");
        elements.stream()
                .map(WebElement::getText)
                .forEach(actualJoiner::add);
        StringJoiner conditionJoiner = new StringJoiner("]\n[", "Conditions: \n[", "].");
        conditions.stream()
                  .map(Condition::toString)
                  .forEach(conditionJoiner::add);

        StringJoiner messageJoiner = new StringJoiner("\n");
        if (ordered)
        {
            messageJoiner.add("Elements don't match conditions.");
        }
        else
        {
            messageJoiner.add("None of the elements matches conditions.");
        }
        messageJoiner.merge(actualJoiner)
                     .merge(conditionJoiner);

        throw UIAssertionError.wrap(collection.driver(),
                new CompositeUIAssertionError(collection.driver(), messageJoiner.toString(), lastError),
                timeoutMs);
    }

    public static class CompositeUIAssertionError
            extends UIAssertionError
    {

        private static final long serialVersionUID = 4133953276828328056L;

        CompositeUIAssertionError(
                Driver driver,
                String message,
                Throwable cause)
        {
            super(driver, message, cause);
        }
    }
}
