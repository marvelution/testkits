/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.utils;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;

import com.codeborne.selenide.*;

/**
 * Utility for {@link Configuration#baseUrl}.
 *
 * @author Mark Rekveld
 * @since 1.4.0
 */
public class BaseUrl
{

    private static final X509TrustManager X_509_TRUST_MANAGER = new X509TrustManager()
    {
        @Override
        public void checkClientTrusted(
                X509Certificate[] chain,
                String authType)
        {
        }

        @Override
        public void checkServerTrusted(
                X509Certificate[] chain,
                String authType)
        {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers()
        {
            return new X509Certificate[]{};
        }
    };

    /**
     * Sets the base {@link #resolveBaseUrl(String) resolved} {@link URI url} specified for {@link Selenide}.
     *
     * @see #setBaseUrl(String)
     * @see #resolveBaseUrl(String)
     * @since 1.5.0
     */
    public static void setBaseUrl(URI url)
    {
        setBaseUrl(url.toASCIIString());
    }

    /**
     * Sets the base {@link #resolveBaseUrl(String) resolved} {@link URI url} specified for {@link Selenide}.
     *
     * @see #setBaseUrl(URI)
     * @see #resolveBaseUrl(String)
     * @since 1.5.0
     */
    public static void setBaseUrl(String url)
    {
        Configuration.baseUrl = BaseUrl.resolveBaseUrl(url);
    }

    /**
     * If a {@link Configuration#remote remote} is specified, then this method tries to replace {@literal localhost} with an address that
     * external system, like Docker container, can use to access the URL, otherwise this method is a no-op.
     */
    public static String resolveBaseUrl(String baseUrl)
    {
        if (Configuration.remote != null && baseUrl.contains("localhost"))
        {
            // On some docker hosts the containers cannot use the loopback address of the host,
            // so we need to lookup an address that they can use.
            try
            {
                Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements())
                {
                    NetworkInterface iface = interfaces.nextElement();
                    try
                    {
                        if (iface.isUp() && !iface.isLoopback())
                        {
                            Enumeration<InetAddress> addresses = iface.getInetAddresses();
                            while (addresses.hasMoreElements())
                            {
                                InetAddress address = addresses.nextElement();
                                String addressedUrl = baseUrl.replace("localhost", address.getHostAddress());
                                try
                                {
                                    if (!address.isLoopbackAddress() && address.isReachable(2000) && isReachable(addressedUrl))
                                    {
                                        return addressedUrl;
                                    }
                                }
                                catch (Exception ignored)
                                {
                                    // try the next address
                                }
                            }
                        }
                    }
                    catch (Exception ignored)
                    {
                        // try the next interface
                    }
                }
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
        return baseUrl;
    }

    private static boolean isReachable(String url)
    {
        if (url.startsWith("https"))
        {
            return isHttpsUrlReachable(url);
        }
        else
        {
            return isHttpUrlReachable(url);
        }
    }

    private static boolean isHttpUrlReachable(String url)
    {
        HttpURLConnection connection = null;
        try
        {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(2000);
            connection.setReadTimeout(2000);
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            return (200 <= responseCode && responseCode <= 399);
        }
        catch (IOException exception)
        {
            return false;
        }
        finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
        }
    }

    private static boolean isHttpsUrlReachable(String url)
    {
        HttpsURLConnection connection = null;
        try
        {
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, new TrustManager[]{X_509_TRUST_MANAGER}, new SecureRandom());
            connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.setHostnameVerifier((hostname, session) -> true);
            connection.setSSLSocketFactory(sslContext.getSocketFactory());
            connection.setConnectTimeout(2000);
            connection.setReadTimeout(2000);
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            return (200 <= responseCode && responseCode <= 399);
        }
        catch (Exception exception)
        {
            return false;
        }
        finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
        }
    }
}
