/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing;

import java.util.*;

import org.marvelution.testing.webdriver.*;

import com.codeborne.selenide.*;
import org.junit.jupiter.api.extension.*;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.*;

/**
 * Test Support for Functional Tests.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExtendWith(WebDriverExtension.class)
public abstract class FunctionalTestSupport
		extends TestSupport
{

	protected void refreshOrOpen(String url)
	{
		if (hasWebDriverStarted() && Objects.equals(url(), url))
		{
			refresh();
		}
		else
		{
			open(url);
		}
	}

	protected void takeScreenShot()
	{
		Screenshots.screenshots.takeScreenshot(WebDriverRunner.driver(), true, true);
	}
}
