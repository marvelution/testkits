<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (c) 2017-present Marvelution Holding B.V.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.marvelution</groupId>
        <artifactId>open-source-parent</artifactId>
        <version>22</version>
    </parent>
    <groupId>org.marvelution.testing</groupId>
    <artifactId>testkits</artifactId>
    <version>1.18-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
        <module>testkit</module>
        <module>functional-testkit</module>
    </modules>

    <name>Testkits Parent</name>
    <description>Commons for test code.</description>
    <inceptionYear>2017</inceptionYear>
    <organization>
        <name>Marvelution Holding B.V.</name>
        <url>https://www.marvelution.com/</url>
    </organization>
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:${git.origin}</connection>
        <developerConnection>scm:git:${git.origin}</developerConnection>
        <url>https://bitbucket.org/marvelution/${git.repository.name}</url>
    </scm>

    <properties>
        <git.repository.name>testkits</git.repository.name>

        <assertj.version>3.25.1</assertj.version>
        <awaitility.version>4.2.0</awaitility.version>
        <commons.lang3.version>3.13.0</commons.lang3.version>
        <commons.fileupload.version>1.5</commons.fileupload.version>
        <guice.version>7.0.0</guice.version>
        <guava.version>32.1.2-jre</guava.version>
        <httpclient.version>5.2.1</httpclient.version>
        <jackson.bom.version>2.15.4</jackson.bom.version>
        <json.smart.version>2.5.0</json.smart.version>
        <junit.jupiter.version>5.10.1</junit.jupiter.version>
        <junit.platform.commons.version>1.10.1</junit.platform.commons.version>
        <logback.version>1.5.6</logback.version>
        <mockito.version>5.4.0</mockito.version>
        <netty.version>4.1.115.Final</netty.version>
        <selenide.version>6.19.1</selenide.version>
        <selenium.version>4.13.0</selenium.version>
        <selenium.htmlunit.version>4.13.0</selenium.htmlunit.version>
        <slf4j.version>2.0.9</slf4j.version>
        <wiremock.version>3.3.1</wiremock.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.marvelution.testing</groupId>
                <artifactId>testkit</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>org.marvelution.testing</groupId>
                <artifactId>functional-testkit</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson</groupId>
                <artifactId>jackson-bom</artifactId>
                <version>${jackson.bom.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback.version}</version>
            </dependency>
            <dependency>
                <groupId>net.minidev</groupId>
                <artifactId>json-smart</artifactId>
                <version>${json.smart.version}</version>
            </dependency>
            <dependency>
                <groupId>org.wiremock</groupId>
                <artifactId>wiremock-standalone</artifactId>
                <version>${wiremock.version}</version>
            </dependency>
            <dependency>
                <groupId>commons-fileupload</groupId>
                <artifactId>commons-fileupload</artifactId>
                <version>${commons.fileupload.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.inject</groupId>
                <artifactId>guice</artifactId>
                <version>${guice.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>${junit.jupiter.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons.lang3.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.httpcomponents.client5</groupId>
                <artifactId>httpclient5</artifactId>
                <version>${httpclient.version}</version>
            </dependency>
            <dependency>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-core</artifactId>
                <version>${assertj.version}</version>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <version>${awaitility.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-junit-jupiter</artifactId>
                <version>${mockito.version}</version>
            </dependency>

            <dependency>
                <groupId>com.codeborne</groupId>
                <artifactId>selenide</artifactId>
                <version>${selenide.version}</version>
            </dependency>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>htmlunit-driver</artifactId>
                <version>${selenium.htmlunit.version}</version>
            </dependency>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-bom</artifactId>
                <version>${selenium.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-bom</artifactId>
                <version>${netty.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <developers>
        <developer>
            <name>Marvelution Holding B.V.</name>
            <organization>Marvelution Holding B.V.</organization>
            <organizationUrl>https://www.marvelution.com</organizationUrl>
            <timezone>+1</timezone>
        </developer>
    </developers>
</project>
