/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.logging;

import java.util.Optional;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * {@link Extension} logging test executions.
 *
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class TestTracerExtension implements BeforeAllCallback, BeforeTestExecutionCallback, AfterTestExecutionCallback {

	private Logger logger;

	@Override
	public void beforeAll(ExtensionContext context) {
		logger = LoggerFactory.getLogger(context.getRequiredTestClass());
	}

	@Override
	public void beforeTestExecution(ExtensionContext context) {
		log("{} STARTING", prefix(context));
	}

	@Override
	public void afterTestExecution(ExtensionContext context) {
		Optional<Throwable> executionException = context.getExecutionException();

		if (executionException.isPresent()) {
			log("{} FAILED", prefix(context), executionException.get());
		} else {
			log("{} SUCCEEDED", prefix(context));
		}
	}

	private String prefix(final ExtensionContext context) {
		return format("TEST %s", context.getDisplayName());
	}

	private void log(String message, Object... args) {
		logger.info(message, args);
	}
}
