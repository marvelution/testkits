/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing;

import java.lang.reflect.Method;

import org.marvelution.testing.logging.TestTracerExtension;
import org.marvelution.testing.wiremock.WireMockExtension;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Support for Tests.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExtendWith({ MockitoExtension.class, WireMockExtension.class, TestTracerExtension.class })
public abstract class TestSupport {

	protected TestInfo testInfo;

	@BeforeEach
	public void testInfo(TestInfo testInfo) {
		this.testInfo = testInfo;
	}

	protected String getTestMethodName() {
		return testInfo.getTestMethod().map(Method::getName).orElse(null);
	}
}
