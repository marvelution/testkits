/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.inject;

import com.google.inject.Guice;
import com.google.inject.Module;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;

import static java.util.Arrays.stream;

/**
 * {@link Extension} implementation that be used for dependency injection using {@link Guice}.
 *
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class InjectorExtension implements BeforeEachCallback {

	private final Module[] modules;

	public InjectorExtension() {
		modules = null;
	}

	public InjectorExtension(Module... modules) {
		this.modules = modules;
	}

	@Override
	public void beforeEach(ExtensionContext context) {
		Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				Object testInstance = context.getRequiredTestInstance();
				if (testInstance instanceof Module) {
					install((Module) testInstance);
				}
				if (modules != null) {
					stream(modules).forEach(this::install);
				}
				requestStaticInjection(context.getRequiredTestClass());
				requestInjection(testInstance);
			}
		});
	}
}
