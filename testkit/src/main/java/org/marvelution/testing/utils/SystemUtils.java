/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;

import static java.nio.file.attribute.PosixFilePermission.GROUP_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.GROUP_READ;
import static java.nio.file.attribute.PosixFilePermission.OTHERS_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OTHERS_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.util.stream.Collectors.toSet;

/**
 * System utilities.
 *
 * @author Mark Rekveld
 * @since 1.2.0
 */
public class SystemUtils {

	private static final String OS_NAME = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);
	private static final Set<PosixFilePermission> FILE_PERMISSIONS = Stream.of(
			OWNER_READ, OWNER_WRITE, OWNER_EXECUTE,
			GROUP_READ, GROUP_EXECUTE,
			OTHERS_READ, OTHERS_EXECUTE
	).collect(toSet());

	/**
	 * Returns the system current specific executable name of the specified {@code name}.
	 */
	public static String getExecutableName(String name) {
		return isWindows() ? name + ".exe" : name;
	}

	/**
	 * Makes the specified {@link Path} executable by trying to set the file permissions to {@literal 755}.
	 */
	public static void makeExecutable(Path path) throws IOException {
		if (!Files.isExecutable(path)) {
			Files.setPosixFilePermissions(path, FILE_PERMISSIONS);
		}
	}

	/**
	 * Returns whether the current system is a Windows based system.
	 */
	public static boolean isWindows() {
		return OS_NAME.contains("win");
	}

	/**
	 * Returns whether the current system is a Mac based system.
	 */
	public static boolean isMac() {
		return OS_NAME.contains("mac");
	}

	/**
	 * Returns whether the current system is a Unix/Linux based system.
	 */
	public static boolean isUnix() {
		return OS_NAME.contains("nix") || OS_NAME.contains("nux");
	}
}
