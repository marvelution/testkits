/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.wiremock;

import java.net.*;

import com.github.tomakehurst.wiremock.client.*;
import com.github.tomakehurst.wiremock.core.*;
import com.github.tomakehurst.wiremock.http.*;
import com.github.tomakehurst.wiremock.matching.*;
import org.junit.jupiter.api.extension.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class WireMockServer
        extends com.github.tomakehurst.wiremock.WireMockServer
        implements ExtensionContext.Store.CloseableResource
{

    public WireMockServer(Options options)
    {
        super(options);
    }

    public static String baseUrl(String baseUrl)
    {
        String stepIp = System.getenv("step_docker_container_ip");
        if (stepIp != null)
        {
            baseUrl = baseUrl.replaceAll("localhost", stepIp);
        }
        return baseUrl;
    }

    public WireMock getClient()
    {
        return client;
    }

    @Override
    public void close()
    {
        shutdown();
    }

    public URI serverUri()
    {
        return URI.create(url("/"));
    }

    @Override
    public String baseUrl()
    {
        return baseUrl(super.baseUrl());
    }

    public void expect(MappingBuilder mappingBuilder)
    {
        client.register(mappingBuilder);
    }

    public void expect(
            RequestMethod method,
            UrlPattern urlPattern,
            ResponseDefinitionBuilder responseDefinitionBuilder)
    {
        client.register(request(method.getName(), urlPattern).willReturn(responseDefinitionBuilder));
    }

    public void verify(
            RequestMethod method,
            UrlPattern urlPattern)
    {
        client.verifyThat(new RequestPatternBuilder(method, urlPattern));
    }

    public void verify(
            int count,
            RequestMethod method,
            UrlPattern urlPattern)
    {
        client.verifyThat(count, new RequestPatternBuilder(method, urlPattern));
    }
}
