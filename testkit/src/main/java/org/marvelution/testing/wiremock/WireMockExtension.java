/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.wiremock;

import java.util.*;

import com.github.tomakehurst.wiremock.client.*;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.*;
import com.github.tomakehurst.wiremock.core.*;
import org.junit.jupiter.api.extension.*;
import org.junit.platform.commons.support.*;

/**
 * {@link ParameterResolver} for supporting WireMock in Junit 5 tests.
 *
 * @author Mark Rekveld
 * @since 1.5.0
 */
public class WireMockExtension
        implements ParameterResolver
{

    private static final ExtensionContext.Namespace NAMESPACE = ExtensionContext.Namespace.create(WireMockExtension.class);

    @Override
    public boolean supportsParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
    {
        Class<?> type = parameterContext.getParameter()
                .getType();
        return WireMockServer.class.isAssignableFrom(type);
    }

    @Override
    public Object resolveParameter(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
    {
        Options options = findAnnotation(parameterContext, extensionContext).map(annotation -> getOptionsFromAnnotation(annotation,
                        extensionContext))
                .orElseGet(WireMockConfiguration::new);

        WireMockServer server = getStore(extensionContext).getOrComputeIfAbsent(
                extensionContext.getUniqueId() + "-" + parameterContext.getIndex(),
                key -> createAndStartWireMockServer(options),
                WireMockServer.class);

        if (WireMock.class.isAssignableFrom(parameterContext.getParameter()
                .getType()))
        {
            return server.getClient();
        }
        else
        {
            return server;
        }
    }

    private ExtensionContext.Store getStore(ExtensionContext extensionContext)
    {
        return extensionContext.getStore(NAMESPACE);
    }

    private Optional<WireMockOptions> findAnnotation(
            ParameterContext parameterContext,
            ExtensionContext extensionContext)
    {
        if (parameterContext.isAnnotated(WireMockOptions.class))
        {
            return parameterContext.findAnnotation(WireMockOptions.class);
        }
        else
        {
            return AnnotationSupport.findAnnotation(extensionContext.getRequiredTestClass(), WireMockOptions.class);
        }
    }

    private Options getOptionsFromAnnotation(
            WireMockOptions options,
            ExtensionContext extensionContext)
    {
        if (Objects.equals(Options.class, options.optionsClass()))
        {
            FileSource fileSource;
            if (options.fileSource()
                        .type() == FileSourceType.UNDER_CLASSPATH)
            {
                fileSource = new ClasspathFileSource(options.fileSource()
                        .path());
            }
            else if (options.fileSource()
                             .type() == FileSourceType.UNDER_PATH)
            {
                fileSource = new SingleRootFileSource(options.fileSource()
                        .path());
            }
            else
            {
                String path = extensionContext.getRequiredTestClass()
                        .getSimpleName();
                if (options.fileSource()
                            .type() == FileSourceType.UNDER_TEST_METHOD)
                {
                    path += "/" + extensionContext.getRequiredTestMethod()
                            .getName();
                }
                fileSource = new ClasspathFileSource(path);
            }
            return new WireMockConfiguration().port(options.port())
                    .fileSource(fileSource);
        }
        else
        {
            try
            {
                return options.optionsClass()
                        .getDeclaredConstructor()
                        .newInstance();
            }
            catch (Exception e)
            {
                throw new ParameterResolutionException("Failed to instantiate " + options.optionsClass(), e);
            }
        }
    }

    private WireMockServer createAndStartWireMockServer(Options options)
    {
        try
        {
            WireMockServer server = new WireMockServer(options);
            server.start();
            return server;
        }
        catch (Exception e)
        {
            throw new ParameterResolutionException("Failed to start WireMock Server", e);
        }
    }
}
