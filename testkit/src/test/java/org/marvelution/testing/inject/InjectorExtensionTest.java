/*
 * Copyright (c) 2017-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.testing.inject;

import org.marvelution.testing.*;

import com.google.inject.Module;
import com.google.inject.*;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;

import static org.assertj.core.api.Assertions.*;

/**
 * Tests for {@link InjectorExtension}.
 *
 * @author Mark Rekveld
 * @since 1.10
 */
@ExtendWith(InjectorExtension.class)
class InjectorExtensionTest
		extends TestSupport
		implements Module
{

	@Inject
	private TestComponent component;
	@Inject
	private TestDependency dependency;

	@Test
	void testWiring()
	{
		assertThat(component).isNotNull()
				.extracting(TestComponent::getDependency)
				.isSameAs(dependency)
				.extracting(TestDependency::ping)
				.isEqualTo("pong");
	}

	@Override
	public void configure(Binder binder)
	{
		binder.bind(TestDependency.class).in(Singleton.class);
	}

	public static class TestComponent
	{

		private final TestDependency dependency;

		@Inject
		public TestComponent(TestDependency dependency)
		{
			this.dependency = dependency;
		}

		public TestDependency getDependency()
		{
			return dependency;
		}
	}

	public static class TestDependency
	{

		String ping()
		{
			return "pong";
		}
	}
}
